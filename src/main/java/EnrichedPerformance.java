public class EnrichedPerformance extends Performance{
    private Play play;
    private int amount;
    private int volumeCredits;
    public EnrichedPerformance(String playID, int audience){
        super(playID,audience);
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }

}
