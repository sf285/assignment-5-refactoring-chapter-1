import java.util.ArrayList;

public class StatementData {
    public String customer;
    public ArrayList<Performance> performances;

    public StatementData(String customer, ArrayList<Performance> performances){
        this.customer = customer;
        this.performances = performances;
    }
}
